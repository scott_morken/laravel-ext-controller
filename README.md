### Laravel Controller Extensions

Contains some helpers for the basic Laravel controller.

`BaseController` contains some minimal functionality, including 
setting up the template view, view names, and subnav key.

Controller with full functionality
```php
<?php
use App\Contracts\Storage\MyProvider;
use App\Http\Requests\Admin\MyFormRequest;
use Illuminate\Foundation\Http\FormRequest;
use Smorken\Ext\Controller\Traits\IndexFiltered;
use Smorken\Ext\Controller\Traits\Crud;

class MyController extends \Smorken\Ext\Controller\BaseController
{
    use IndexFiltered, Crud {
        doSave as traitDoSave;
    }
    
    /**
     * Base view "path". View name of route is appended to create
     * the complete view name
     */
    protected $base_view = 'admin.my';

    /**
     * Optional property that is passed to the view (can
     * be used to dynamically include a content layout
     * in a master blade template 
     */
    protected $template = 'layouts.templates.two_col';

    public function __construct(MyProvider $provider) {
        $this->setProvider($provider);
        parent::__construct();
    }

    public function doSave(MyFormRequest $request, $id = null)
    {
        return $this->traitDoSave($request, $id);
    }
    
    protected function getAttributes(FormRequest $request)
    {
        return $request->only(['foo', 'bar']);
    }
}
```

This example controller would depend on views named like:
  * /admin/my
      * create.blade.php
      * delete.blade.php
      * index.blade.php
      * update.blade.php
      * view.blade.php
      
##### Traits

`Actions`: contains functionality to add actions to views.

`Cache`: contains functionality to flush the cache.

`Core`: contains the core functionality, setting up the shared variables, views names, etc. Contains generic
get route builder.

`Create`: contains `create` get route

`Crud`: includes all of the traits needed for basic
CRUD operations

`Delete`: contains `delete` and `doDelete` routes

`Filter`: contains filter creation functionality

`Index`: contains `index` route

`IndexFiltered`: contains `index` route with filter

`Model`: contains `loadModel` method

`Provider`: contains methods to handle a provider

`Save`: contains `doSave` post route

`Update`: contains `update` get route

`View`: contains `view` get route

<?php

namespace Smorken\Ext\Controller\Traits;

use Smorken\CacheAssist\CacheOptions;
use Smorken\CacheAssist\Contracts\CacheAssist;

trait Cache
{

    /**
     * @var CacheAssist
     */
    protected $cacheAssist;

    protected function clearCacheItem($key)
    {
        $this->getCacheAssist()->forget($key);
    }

    protected function createCacheAssist(array $cacheOptions): CacheAssist
    {
        $cacheAssist = $this->getCacheAssistFromProvider();
        if (!$cacheAssist) {
            $cacheAssist = new \Smorken\CacheAssist\CacheAssist(
                new CacheOptions(
                    [
                        'baseName' => get_class(),
                    ]
                )
            );
        }
        $cacheAssist->getCacheOptions()->setOptions($cacheOptions);
        return $cacheAssist;
    }

    protected function flushCache()
    {
        $this->getCacheAssist()->forgetAuto();
    }

    protected function getCacheAssist(array $cacheOptions = []): CacheAssist
    {
        if (!$this->cacheAssist) {
            $this->cacheAssist = $this->createCacheAssist($cacheOptions);
        }
        return $this->cacheAssist;
    }

    protected function getCacheAssistFromProvider(): ?CacheAssist
    {
        if (property_exists($this, 'provider') && method_exists($this->provider, 'getCacheAssist')) {
            return $this->provider->getCacheAssist();
        }
        return null;
    }
}

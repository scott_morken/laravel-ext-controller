<?php


namespace Smorken\Ext\Controller\Traits;


trait Authorize
{

    protected function authorizeCreate($model)
    {
        return true;
    }

    protected function authorizeDelete($model)
    {
        return true;
    }

    protected function authorizeIndex($model)
    {
        return true;
    }

    protected function authorizeUpdate($model)
    {
        return true;
    }

    protected function authorizeView($model)
    {
        return true;
    }
}

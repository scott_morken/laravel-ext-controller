<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 5/3/18
 * Time: 9:06 AM
 */

namespace Smorken\Ext\Controller\Traits;

use Illuminate\Foundation\Http\FormRequest;

trait Save
{

    /**
     * @param  \Illuminate\Foundation\Http\FormRequest  $request
     * @param  null  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function doSave(FormRequest $request, $id = null)
    {
        if ($id) {
            return $this->handleUpdate($request, $id);
        }
        return $this->handleCreate($request);
    }

    protected function createModel(FormRequest $request)
    {
        $this->tryToAuthorize('create', $this->getProvider()->getModel());
        return $this->getProvider()->create($this->getAttributesCreate($request));
    }

    protected function getAttributes(FormRequest $request)
    {
        return $request->all();
    }

    protected function getAttributesCreate(FormRequest $request)
    {
        return $this->getAttributes($request);
    }

    protected function getAttributesUpdate(FormRequest $request, $id)
    {
        return $this->getAttributes($request);
    }

    protected function getParams($request)
    {
        if (method_exists($this, 'getFilter')) {
            return $this->getFilter($request)
                        ->all();
        }
        return [];
    }

    protected function handleCreate(FormRequest $request)
    {
        $model = $this->createModel($request);
        if ($model) {
            $this->onPostCreateSuccess($request, $model);
            return $this->redirectOnSuccessSaveCreate($request, $model);
        }
        $this->onPostCreateFailure($request);
        return $this->redirectOnFailureSaveCreate($request);
    }

    protected function handleUpdate(FormRequest $request, $id)
    {
        $model = $this->findModel($id);
        if ($this->updateModel($request, $id, $model)) {
            $this->onPostUpdateSuccess($request, $model, $id);
            return $this->redirectOnSuccessSaveUpdate($request, $model);
        }
        $this->onPostUpdateFailure($request, $id);
        return $this->redirectOnFailureSaveUpdate($request, $model);
    }

    protected function onPostCreateFailure(FormRequest $request)
    {
        $this->onSaveFailure($request);
    }

    protected function onPostCreateSuccess(FormRequest $request, $model)
    {
        $this->onSaveSuccess($request, $model);
    }

    protected function onPostUpdateFailure(FormRequest $request, $id)
    {
        $this->onSaveFailure($request);
    }

    protected function onPostUpdateSuccess(FormRequest $request, $model, $id)
    {
        $this->onSaveSuccess($request, $model);
    }

    protected function onSaveFailure(FormRequest $request)
    {
        // override if needed
    }

    protected function onSaveSuccess(FormRequest $request, $model)
    {
        // override if needed
    }

    /**
     * @param  \Illuminate\Foundation\Http\FormRequest  $request
     * @param  null  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectOnFailureSave(FormRequest $request, $model = null)
    {
        $msg = sprintf('Error saving resource [%s].',
            $model && method_exists($model, 'getKey') ? $model->getKey() : 'new');
        return back()
            ->withInput()
            ->withErrors(['save-error' => [$msg]]);
    }

    /**
     * @param  \Illuminate\Foundation\Http\FormRequest  $request
     * @param  null  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectOnFailureSaveCreate(FormRequest $request, $model = null)
    {
        return $this->redirectOnFailureSave($request, $model);
    }

    /**
     * @param  \Illuminate\Foundation\Http\FormRequest  $request
     * @param  null  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectOnFailureSaveUpdate(FormRequest $request, $model = null)
    {
        return $this->redirectOnFailureSave($request, $model);
    }

    /**
     * @param  \Illuminate\Foundation\Http\FormRequest  $request
     * @param  null  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectOnSuccessSave(FormRequest $request, $model = null)
    {
        $request->session()
                ->flash('flash:success', sprintf('Saved resource [%s].',
                    $model && method_exists($model, 'getKey') ? $model->getKey() : 'new'));
        return redirect()->action([$this->getController(), 'index'], $this->getParams($request));
    }

    /**
     * @param  \Illuminate\Foundation\Http\FormRequest  $request
     * @param  null  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectOnSuccessSaveCreate(FormRequest $request, $model = null)
    {
        return $this->redirectOnSuccessSave($request, $model);
    }

    /**
     * @param  \Illuminate\Foundation\Http\FormRequest  $request
     * @param  null  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectOnSuccessSaveUpdate(FormRequest $request, $model = null)
    {
        return $this->redirectOnSuccessSave($request, $model);
    }

    protected function updateModel(FormRequest $request, $id, $model)
    {
        $this->tryToAuthorize('update', $model);
        return $this->getProvider()->update($model, $this->getAttributesUpdate($request, $id));
    }
}

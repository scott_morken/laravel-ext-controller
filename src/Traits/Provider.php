<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/7/16
 * Time: 10:49 AM
 */

namespace Smorken\Ext\Controller\Traits;

trait Provider
{

    /**
     * @var object provider for controller
     */
    protected $provider;

    /**
     * @return object
     */
    protected function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param $provider
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 5/3/18
 * Time: 9:01 AM
 */

namespace Smorken\Ext\Controller\Traits;

use Illuminate\Http\Request;

trait Update
{

    public function update(Request $request, $id)
    {
        return $this->get($request, 'update', $id);
    }
}

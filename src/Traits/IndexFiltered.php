<?php

namespace Smorken\Ext\Controller\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

trait IndexFiltered
{

    public function index(Request $request)
    {
        $filter = $this->getFilterIfExists($request, null);
        $models = new Collection();
        if (method_exists($this, 'getProvider')) {
            $models = $this->getProvider()
                           ->getByFilter($filter);
            $this->tryToAuthorize('index', $this->getProvider()->getModel());
        }
        return \Illuminate\Support\Facades\View::make($this->getView('index'))
                                               ->with('models', $models)
                                               ->with('filter', $filter)
                                               ->with('actions', $this->getActionsIfExists($request, null));
    }
}

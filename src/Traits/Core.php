<?php

namespace Smorken\Ext\Controller\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Smorken\Ext\Controller\ControllerException;

trait Core
{

    protected function actionArray($method)
    {
        return [$this->getController(), $method];
    }

    protected function addControllerToVariables()
    {
        \Illuminate\Support\Facades\View::share('controller', $this->getController());
    }

    protected function addTemplateToVariables()
    {
        if (property_exists($this, 'template')) {
            \Illuminate\Support\Facades\View::share('template', $this->template);
        }
    }

    protected function get(Request $request, $view, $id = null)
    {
        $model = $this->getModelIfExists($id);
        $this->tryToAuthorize($view, $model);
        return \Illuminate\Support\Facades\View::make($this->getView($view))
                                               ->with('model', $model)
                                               ->with('filter', $this->getFilterIfExists($request, $id))
                                               ->with('actions', $this->getActionsIfExists($request, $id));
    }

    protected function getActionsIfExists(Request $request, $id)
    {
        $actions = [];
        if (method_exists($this, 'actions')) {
            $actions = $this->actions($id, $request);
        }
        return $actions;
    }

    protected function getController()
    {
        return get_called_class();
    }

    protected function getFilterIfExists($request, $id)
    {
        $filter = new \Smorken\Support\Filter();
        if (method_exists($this, 'getFilter')) {
            $filter = $this->getFilter($request);
        }
        return $filter;
    }

    protected function getModelIfExists($id)
    {
        $model = null;
        if ($id) {
            if (method_exists($this, 'findModel')) {
                $model = $this->findModel($id);
            }
        } else {
            if (method_exists($this, 'getProvider')) {
                $model = $this->getProvider()->getModel();
            }
        }
        $model = $this->modifyModelBeforeUse($model);
        return $model;
    }

    /**
     * A map of a loaded trait to its required siblings
     *
     * @return array
     */
    protected function getTraitMap()
    {
        $mp = [
            Model::class,
            Provider::class,
        ];
        return [
            Create::class => $mp,
            Delete::class => $mp,
            Index::class => $mp,
            IndexFiltered::class => $mp,
            Model::class => [
                Provider::class,
            ],
            Save::class => $mp,
            Update::class => $mp,
            View::class => $mp,
        ];
    }

    protected function getView($name): string
    {
        $base_view = '';
        if (property_exists($this, 'base_view')) {
            $base_view = Str::finish($this->base_view, '.');
        }
        if (Str::startsWith($name, '.')) {
            Str::replaceFirst('.', '', $name);
        }
        return $base_view.$name;
    }

    protected function init()
    {
        $this->verifyTraits();
        $this->addControllerToVariables();
        $this->addTemplateToVariables();
    }

    protected function modifyModelBeforeUse($model)
    {
        return $model;
    }

    protected function tryToAuthorize($which, $model)
    {
        $methods = [
            'create' => 'authorizeCreate', 'delete' => 'authorizeDelete', 'index' => 'authorizeIndex',
            'update' => 'authorizeUpdate',
            'view' => 'authorizeView',
            'doCreate' => 'authorizeCreate', 'doDelete' => 'authorizeDelete', 'doUpdate' => 'authorizeUpdate',
        ];
        $method = $methods[$which] ?? null;
        if ($method && $model && method_exists($this, $method)) {
            $this->$method($model);
        }
    }

    /**
     * Implemented because PHP < 7.3 doesn't handle overloaded traits. Not
     * a great solution here.
     *
     * @throws \Smorken\Ext\Controller\ControllerException
     */
    protected function verifyTraits()
    {
        $tmap = $this->getTraitMap();
        $loaded = class_uses_recursive($this);
        foreach ($tmap as $trait => $required) {
            if (in_array($trait, $loaded)) {
                $this->verifyTraitsLoaded($loaded, $required);
            }
        }
    }

    protected function verifyTraitsLoaded($all, $required)
    {
        foreach ($required as $trait) {
            if (!in_array($trait, $all)) {
                throw new ControllerException("$trait is required.");
            }
        }
    }
}

<?php

namespace Smorken\Ext\Controller\Traits;

trait Model
{

    use Provider;

    protected function findModel($id)
    {
        return $this->getProvider()
                    ->findOrFail($id);
    }
}

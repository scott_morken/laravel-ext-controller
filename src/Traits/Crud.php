<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 5/3/18
 * Time: 9:22 AM
 */

namespace Smorken\Ext\Controller\Traits;

trait Crud
{

    use Cache, Core, Create, Delete, Save, Update, View, Model;
}

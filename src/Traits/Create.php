<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 5/3/18
 * Time: 8:55 AM
 */

namespace Smorken\Ext\Controller\Traits;

use Illuminate\Http\Request;

trait Create
{

    public function create(Request $request)
    {
        return $this->get($request, 'create');
    }
}

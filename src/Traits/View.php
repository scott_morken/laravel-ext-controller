<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 5/3/18
 * Time: 9:00 AM
 */

namespace Smorken\Ext\Controller\Traits;

use Illuminate\Http\Request;

trait View
{

    public function view(Request $request, $id)
    {
        return $this->get($request, 'view', $id);
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/7/16
 * Time: 10:42 AM
 */

namespace Smorken\Ext\Controller\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

trait Index
{

    public function index(Request $request)
    {
        $models = new Collection();
        if (method_exists($this, 'getProvider')) {
            $models = $this->getProvider()
                           ->all();
            $this->tryToAuthorize('index', $this->getProvider()->getModel());
        }
        return \Illuminate\Support\Facades\View::make($this->getView('index'))
                                               ->with('models', $models)
                                               ->with('actions', $this->getActionsIfExists($request, null));
    }
}

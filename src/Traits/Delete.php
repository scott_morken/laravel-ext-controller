<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 5/3/18
 * Time: 9:02 AM
 */

namespace Smorken\Ext\Controller\Traits;

use Illuminate\Http\Request;

trait Delete
{

    public function delete(Request $request, $id)
    {
        return $this->get($request, 'delete', $id);
    }

    public function doDelete(Request $request, $id)
    {
        if ($this->deleteModel($request, $id)) {
            return $this->redirectOnSuccessDelete($request, $id);
        }
        return $this->redirectOnFailureDelete($request, $id);
    }

    protected function deleteModel(Request $request, $id)
    {
        $model = $this->findModel($id);
        $this->tryToAuthorize('delete', $model);
        return $this->getProvider()
                    ->delete($model);
    }

    protected function redirectOnFailureDelete(Request $request, $id)
    {
        $msg = sprintf("Resource [%s] NOT deleted.", $id);
        return redirect()
            ->action([$this->getController(), 'index'], $this->getFilterIfExists($request, $id)
                                                             ->all())
            ->withErrors(['delete-error' => [$msg]]);
    }

    protected function redirectOnSuccessDelete(Request $request, $id)
    {
        $request->session()
                ->flash('flash:success', sprintf("Resource [%s] deleted.", $id));
        return redirect()->action([$this->getController(), 'index'], $this->getFilterIfExists($request, $id)
                                                                          ->all());
    }
}

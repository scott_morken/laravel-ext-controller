<?php

namespace Smorken\Ext\Controller\Traits;

use Illuminate\Http\Request;

trait Actions
{

    protected function actions($id = null, Request $request = null)
    {
        $params = method_exists($this, 'getFilter') ? $this->getFilter($request)
                                                           ->toArray() : [];
        $actions = [];
        $this->addAction($actions, 'Create', 'create', $params);
        $this->addAction($actions, 'List', 'index', $params);
        if ($id) {
            $actions = array_merge($actions, $this->actionsWithId($id, $request));
        }
        return $actions;
    }

    protected function actionsWithId($id, Request $request = null, $output_text = false)
    {
        $params = array_merge(['id' => $id], method_exists($this, 'getFilter') ? $this->getFilter($request)
                                                                                      ->toArray() : []);
        $actions = [];
        $this->addAction($actions, 'View', 'view', $params, $this->getActionIcon('view', 'text-primary fa fa-eye'),
            $output_text);
        $this->addAction($actions, 'Update', 'update', $params,
            $this->getActionIcon('update', 'text-success fa fa-pencil'), $output_text);
        $this->addAction($actions, 'Delete', 'delete', $params,
            $this->getActionIcon('delete', 'text-danger fa fa-trash'), $output_text);
        return $actions;
    }

    protected function addAction(&$actions, $title, $action, $params, $icon = null, $output_text = null)
    {
        $url = $this->getActionUrl($action, $params);
        if ($url !== false) {
            $actions[$title] = [
                'url'         => $url,
                'icon'        => $icon,
                'output_text' => (bool) $output_text,
            ];
        }
    }

    protected function getActionIcon($action, $default = null)
    {
        return config('ext-controller.icons.'.$action, $default);
    }

    protected function getActionUrl($action, $params)
    {
        if (method_exists($this, $action)) {
            try {
                return action([$this->getController(), $action], $params);
            } catch (\Exception $e) {
                // do nothing, fall through to false
            }
        }
        return false;
    }
}

<?php

namespace Smorken\Ext\Controller\Traits;

trait Filter
{
    /**
     * @param  \Illuminate\Http\Request|\Illuminate\Foundation\Http\FormRequest  $request
     * @return \Smorken\Support\Contracts\Filter
     */
    protected function getFilter($request): \Smorken\Support\Contracts\Filter
    {
        return new \Smorken\Support\Filter();
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/24/14
 * Time: 6:58 AM
 */

namespace Smorken\Ext\Controller;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use Smorken\Ext\Controller\Traits\Core;

class BaseController extends Controller
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, Core;

    /**
     * @var string base path name of view that controller uses ('base')
     */
    protected $base_view = '';

    /**
     * @var string Subnav lookup string
     */
    protected $subnav = '';

    public function __construct()
    {
        $this->init();
    }
}

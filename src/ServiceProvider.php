<?php namespace Smorken\Ext\Controller;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootConfig();
    }

    protected function bootConfig()
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'ext-controller');
        $this->publishes([$config => config_path('ext-controller.php')], 'config');
    }
}

<?php


namespace Tests\Smorken\Ext\Controller\e2e;

use Illuminate\Auth\GenericUser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Smorken\Ext\Controller\BaseController;
use Smorken\Ext\Controller\Traits\Authorize;
use Smorken\Ext\Controller\Traits\Full;
use Smorken\Ext\Controller\Traits\IndexFiltered;
use Mockery as m;
use Tests\Smorken\Ext\Controller\ProjectTest;

class AModel
{

    public $id = 99;

    public $user_id = 1;
}

class APolicy
{
    public function create(GenericUser $user)
    {
        return $user->id === 1;
    }

    public function delete(GenericUser $user, AModel $model)
    {
        return $user->id === $model->user_id;
    }

    public function update(GenericUser $user, AModel $model)
    {
        return $user->id === $model->user_id;
    }

    public function view(GenericUser $user, AModel $model)
    {
        return $user->id === $model->user_id;
    }
}

class AProvider
{

}

class AuthorizeTestController extends BaseController
{

    use Authorize, IndexFiltered, Full {
        doSave as traitDoSave;
    }

    protected $base_view = 'smorken/ext-controller::testing';

    public function __construct(AProvider $provider)
    {
        $this->setProvider($provider);
        parent::__construct();
    }

    public function doSave(AuthorizeTestFormRequest $request, $id = null)
    {
        return $this->traitDoSave($request, $id);
    }

    public function viewName($view = null)
    {
        return $this->getView($view);
    }

    protected function authorizeCreate($model)
    {
        return $this->authorize('create', $model);
    }

    protected function authorizeDelete($model)
    {
        return $this->authorize('delete', $model);
    }

    protected function authorizeIndex($model)
    {
        return true;
    }

    protected function authorizeUpdate($model)
    {
        return $this->authorize('update', $model);
    }

    protected function authorizeView($model)
    {
        return $this->authorize('view', $model);
    }

    protected function findModel($id)
    {
        return new AModel();
    }

    protected function getData(Request $r)
    {
        return $r->only(['foo', 'fiz']);
    }
}

class AuthorizeTestFormRequest extends FormRequest
{

    public function rules()
    {
        return [];
    }
}

class AuthorizeTraitControllerTest extends ProjectTest
{

    /**
     * @var m\Mock
     */
    protected $provider;

    public function mockSimple()
    {
        $mocked = m::mock('StdClass');
        return $mocked;
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->provider = m::mock(AProvider::class);
        $this->app[AProvider::class] = $this->provider;
        $this->provider->shouldReceive('getModel')->andReturn(new AModel());
        $this->addRoutes();
        Gate::policy(AModel::class, APolicy::class);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testDeleteDeleteIsAllowed()
    {
        $this->authenticate(1);
        $this->provider->shouldReceive('delete')
                       ->once()
                       ->with(m::type(AModel::class))
                       ->andReturn(true);
        $response = $this->delete('delete/1');
        $response->assertSessionHas('flash:success')
                 ->assertRedirect('');
    }

    public function testDeleteDeleteIsDenied()
    {
        $this->authenticate(2);
        $this->provider->shouldReceive('delete')
                       ->never();
        $response = $this->delete('delete/1');
        $response->assertForbidden();
    }

    public function testGetCreateIsAllowed()
    {
        $this->authenticate(1);
        $response = $this->get('create');
        $response->assertSee('Create');
        $response->assertSee('Form');
        $response->assertSuccessful();
    }

    public function testGetCreateIsDenied()
    {
        $this->authenticate(2);
        $response = $this->get('create');
        $response->assertForbidden();
    }

    public function testGetDeleteIsAllowed()
    {
        $this->authenticate(1);
        $response = $this->get('delete/1');
        $response->assertSuccessful();
    }

    public function testGetDeleteIsDenied()
    {
        $this->authenticate(2);
        $response = $this->get('delete/1');
        $response->assertForbidden();
    }

    public function testGetIndexIsAllowedForAnyone()
    {
        $this->provider->shouldReceive('getByFilter')->andReturn(new Collection());
        $response = $this->get('');
        $response->assertSuccessful();
    }

    public function testGetUpdateIsAllowed()
    {
        $this->authenticate(1);
        $response = $this->get('update/1');
        $response->assertSee('Update');
        $response->assertSee('Form');
        $response->assertSuccessful();
    }

    public function testGetUpdateIsDenied()
    {
        $this->authenticate(2);
        $response = $this->get('update/1');
        $response->assertForbidden();
    }

    public function testGetViewIsAllowed()
    {
        $this->authenticate(1);
        $response = $this->get('view/1');
        $response->assertSuccessful();
    }

    public function testGetViewIsDenied()
    {
        $this->authenticate(2);
        $response = $this->get('view/1');
        $response->assertForbidden();
    }

    public function testPostCreateIsAllowed()
    {
        $this->authenticate(1);
        $this->provider->shouldReceive('create')
                       ->with(['foo' => 'bar', 'fiz' => 'buz'])
                       ->andReturn(new AModel());
        $response = $this->post('save', ['foo' => 'bar', 'fiz' => 'buz']);
        $response->assertRedirect('');
        $response->assertSessionHas('flash:success');
    }

    public function testPostCreateIsDenied()
    {
        $this->authenticate(2);
        $this->provider->shouldReceive('create')
                       ->never();
        $response = $this->post('save', ['foo' => 'bar', 'fiz' => 'buz']);
        $response->assertForbidden();
    }

    public function testPostUpdateIsAllowed()
    {
        $this->authenticate(1);
        $m = new AModel();
        $this->provider->shouldReceive('update')
                       ->once()
                       ->with(m::type(AModel::class), ['foo' => 'bar', 'fiz' => 'buz'])
                       ->andReturn($m);
        $response = $this->post('save/1', ['foo' => 'bar', 'fiz' => 'buz']);
        $response->assertSessionHas('flash:success')
                 ->assertRedirect('');
    }

    public function testPostUpdateIsDenied()
    {
        $this->authenticate(2);
        $m = new AModel();
        $this->provider->shouldReceive('update')
                       ->never();
        $response = $this->post('save/1', ['foo' => 'bar', 'fiz' => 'buz']);
        $response->assertForbidden();
    }

    protected function addRoutes()
    {
        Route::group(['middleware' => ['web']], function () {
            Route::get('', '\Tests\Smorken\Ext\Controller\e2e\AuthorizeTestController@index');
            Route::get('view/{id}', '\Tests\Smorken\Ext\Controller\e2e\AuthorizeTestController@view');
            Route::get('create', '\Tests\Smorken\Ext\Controller\e2e\AuthorizeTestController@create');
            Route::post('save/{id?}', '\Tests\Smorken\Ext\Controller\e2e\AuthorizeTestController@doSave');
            Route::get('update/{id}', '\Tests\Smorken\Ext\Controller\e2e\AuthorizeTestController@update');
            Route::get('delete/{id}', '\Tests\Smorken\Ext\Controller\e2e\AuthorizeTestController@delete');
            Route::delete('delete/{id}', '\Tests\Smorken\Ext\Controller\e2e\AuthorizeTestController@doDelete');
        });
    }

    protected function authenticate($user_id)
    {
        $user = new GenericUser(['id' => $user_id]);
        $this->actingAs($user);
    }
}

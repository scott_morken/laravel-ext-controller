<?php

namespace Tests\Smorken\Ext\Controller\e2e;

use Illuminate\Support\Facades\Route;
use Mockery as m;
use Orchestra\Testbench\TestCase;
use Smorken\Ext\Controller\BaseController;
use Smorken\Ext\Controller\Traits\Delete;
use Smorken\Ext\Controller\Traits\Model;

class PTController extends BaseController
{

    use Delete, Model;

    protected $base_view = 'smorken/ext-controller::testing';

    public function __construct(PTProvider $provider)
    {
        $this->setProvider($provider);
        parent::__construct();
    }

    public function index()
    {
        return response('foo');
    }
}

class PTProvider
{

}

class PartialTraitedControllerTest extends TestCase
{

    /**
     * @var m\Mock
     */
    protected $provider;

    public function mockSimple()
    {
        $mocked = m::mock('StdClass');
        return $mocked;
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->provider = m::mock(PTProvider::class);
        $this->app[PTProvider::class] = $this->provider;
        $this->addRoutes();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testDeleteDelete()
    {
        $model = $this->mockSimple();
        $provider = $this->provider;
        $provider->shouldReceive('findOrFail')
                 ->once()
                 ->with(1)
                 ->andReturn($model);
        $provider->shouldReceive('delete')
                 ->once()
                 ->with($model)
                 ->andReturn(true);
        $response = $this->delete('delete/1');
        $response->assertSessionHas('flash:success')
                 ->assertRedirect('');
    }

    protected function addRoutes()
    {
        Route::group(['middleware' => ['web']], function () {
            Route::get('', '\Tests\Smorken\Ext\Controller\e2e\PTController@index');
            Route::get('delete/{id}', '\Tests\Smorken\Ext\Controller\e2e\PTController@delete');
            Route::delete('delete/{id}', '\Tests\Smorken\Ext\Controller\e2e\PTController@doDelete');
        });
    }
}

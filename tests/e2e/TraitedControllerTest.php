<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/24/14
 * Time: 7:40 AM
 */

namespace Tests\Smorken\Ext\Controller\e2e;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Mockery as m;
use Smorken\Ext\Controller\BaseController;
use Smorken\Ext\Controller\Traits\Full;
use Smorken\Ext\Controller\Traits\IndexFiltered;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tests\Smorken\Ext\Controller\ProjectTest;

class CProvider
{

}

class TestController extends BaseController
{

    use IndexFiltered, Full {
        doSave as traitDoSave;
    }

    protected $base_view = 'smorken/ext-controller::testing';

    public function __construct(CProvider $provider)
    {
        $this->setProvider($provider);
        parent::__construct();
    }

    public function doSave(TestFormRequest $request, $id = null)
    {
        return $this->traitDoSave($request, $id);
    }

    public function viewName($view = null)
    {
        return $this->getView($view);
    }

    protected function getData(Request $r)
    {
        return $r->only(['foo', 'fiz']);
    }
}

class TestFormRequest extends FormRequest
{

    public function rules()
    {
        return [];
    }
}

class TraitedControllerTest extends ProjectTest
{

    /**
     * @var m\Mock
     */
    protected $provider;

    public function mockSimple()
    {
        $mocked = m::mock('StdClass');
        return $mocked;
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->provider = m::mock(CProvider::class);
        $this->app[CProvider::class] = $this->provider;
        $this->addRoutes();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testDeleteDelete()
    {
        $model = $this->mockSimple();
        $provider = $this->provider;
        $provider->shouldReceive('findOrFail')
                 ->once()
                 ->with(1)
                 ->andReturn($model);
        $provider->shouldReceive('delete')
                 ->once()
                 ->with($model)
                 ->andReturn(true);
        $response = $this->delete('delete/1');
        $response->assertSessionHas('flash:success')
                 ->assertRedirect('');
    }

    public function testDeleteDeleteFails()
    {
        $model = $this->mockSimple();
        $provider = $this->provider;
        $provider->shouldReceive('findOrFail')
                 ->once()
                 ->with(1)
                 ->andReturn($model);
        $provider->shouldReceive('delete')
                 ->once()
                 ->with($model)
                 ->andReturn(false);
        $provider->shouldReceive('getModel')
                 ->never();
        $provider->shouldReceive('find')
                 ->never();
        $response = $this->delete('delete/1');
        $response->assertSessionHasErrors('delete-error');
        $response->assertRedirect('');
    }

    public function testGetCreate()
    {
        $this->provider->shouldReceive('getModel')
                       ->once()
                       ->andReturn($this->mockSimple());
        $response = $this->get('create');
        $response->assertSee('Create');
        $response->assertSee('Form');
        $response->assertSuccessful();
    }

    public function testGetDelete()
    {
        $model = $this->mockSimple();
        $provider = $this->provider;
        $provider->shouldReceive('findOrFail')
                 ->once()
                 ->with(1)
                 ->andReturn($model);
        $provider->shouldReceive('getModel')
                 ->never();
        //$this->sut->setBase('smorken/controller-traited::testing');
        $response = $this->get('delete/1');
        $response->assertSuccessful()
                 ->assertSee('DELETE');
    }

    public function testGetIndex()
    {
        $this->provider->shouldReceive('getByFilter')
                       ->once()
                       ->andReturn([]);
        $this->provider->shouldReceive('getModel')
            ->once()
            ->andReturn('Foo');
        $response = $this->get('/');
        $response->assertSuccessful();
        $response->assertSeeText('foo index');
    }

    public function testGetUpdate()
    {
        $model = $this->mockSimple();
        $provider = $this->provider;
        $provider->shouldReceive('findOrFail')
                 ->once()
                 ->with(1)
                 ->andReturn($model);
        $provider->shouldReceive('getModel')
                 ->never();
        $response = $this->get('update/1');
        $response->assertSuccessful();
        $response->assertSee('Update')
                 ->assertSee('Form');
    }

    public function testGetView()
    {
        $model = $this->mockSimple();
        $this->provider->shouldReceive('findOrFail')
                       ->once()
                       ->with(1)
                       ->andReturn($model);
        $response = $this->get('view/1');
        $response->assertSuccessful();
    }

    public function testGetViewName()
    {
        $sut = new TestController($this->provider);
        $this->assertEquals('smorken/ext-controller::testing.index', $sut->viewName('index'));
    }

    public function testLoadModelNotFoundIs404()
    {
        $provider = $this->provider;
        $provider->shouldReceive('findOrFail')
                 ->once()
                 ->with(1)
                 ->andThrow(new NotFoundHttpException('Nope'));
        $provider->shouldReceive('getModel')
                 ->never();
        $response = $this->get('update/1');
        $response->assertStatus(404)
                 ->assertSee('Not Found');
    }

    public function testPostCreate()
    {
        $model = $this->mockSimple();
        $provider = $this->provider;
        $provider->shouldReceive('getModel')
            ->once()
            ->andReturn('Foo');
        $provider->shouldReceive('create')
                 ->with(['foo' => 'bar', 'fiz' => 'buz'])
                 ->andReturn($model);
        $response = $this->post('save', ['foo' => 'bar', 'fiz' => 'buz']);
        $response->assertRedirect('');
        $response->assertSessionHas('flash:success');
    }

    public function testPostCreateWithErrors()
    {
        $provider = $this->provider;
        $provider->shouldReceive('getModel')
                 ->once()
                 ->andReturn('Foo');
        $provider->shouldReceive('create')
                 ->with(['foo' => 'bar', 'fiz' => 'buz'])
                 ->andReturn(false);
        $response = $this->post('save', ['foo' => 'bar', 'fiz' => 'buz']);
        $response->assertSessionHasErrors('save-error');
        $response->assertSessionHasInput(['foo' => 'bar', 'fiz' => 'buz']);
    }

    public function testPostUpdate()
    {
        $model = $this->mockSimple();
        $provider = $this->provider;
        $provider->shouldReceive('findOrFail')
                 ->once()
                 ->with(1)
                 ->andReturn($model);
        $provider->shouldReceive('update')
                 ->once()
                 ->with($model, ['foo' => 'bar', 'fiz' => 'buz'])
                 ->andReturn($model);
        $response = $this->post('save/1', ['foo' => 'bar', 'fiz' => 'buz']);
        $response->assertSessionHas('flash:success')
                 ->assertRedirect('');
    }

    public function testPostUpdateWithErrors()
    {
        $model = $this->mockSimple();
        $provider = $this->provider;
        $provider->shouldReceive('findOrFail')
                 ->once()
                 ->with(1)
                 ->andReturn($model);
        $provider->shouldReceive('update')
                 ->with($model, ['foo' => 'bar', 'fiz' => 'buz'])
                 ->andReturn(false);
        $response = $this->post('save/1', ['foo' => 'bar', 'fiz' => 'buz']);
        $response->assertSessionHasErrors('save-error');
        $response->assertSessionHasInput(['foo' => 'bar', 'fiz' => 'buz']);
    }

    protected function addRoutes()
    {
        Route::group(['middleware' => ['web']], function () {
            Route::get('no-provider', '\Tests\Smorken\Ext\Controller\e2e\TestControllerNoProvider@index');
            Route::get('', '\Tests\Smorken\Ext\Controller\e2e\TestController@index');
            Route::post('', '\Tests\Smorken\Ext\Controller\e2e\TestController@doIndex');
            Route::get('view/{id}', '\Tests\Smorken\Ext\Controller\e2e\TestController@view');
            Route::post('view/{id}', '\Tests\Smorken\Ext\Controller\e2e\TestController@doView');
            Route::get('create', '\Tests\Smorken\Ext\Controller\e2e\TestController@create');
            Route::post('save/{id?}', '\Tests\Smorken\Ext\Controller\e2e\TestController@doSave');
            Route::get('update/{id}', '\Tests\Smorken\Ext\Controller\e2e\TestController@update');
            Route::get('delete/{id}', '\Tests\Smorken\Ext\Controller\e2e\TestController@delete');
            Route::delete('delete/{id}', '\Tests\Smorken\Ext\Controller\e2e\TestController@doDelete');

            Route::get('no-package', '\Tests\Smorken\Ext\Controller\e2e\TestController@noViewFound');
        });
    }
}

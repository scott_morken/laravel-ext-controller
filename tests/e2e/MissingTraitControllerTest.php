<?php

namespace Tests\Smorken\Ext\Controller\e2e;

use Illuminate\Support\Facades\Route;
use Mockery as m;
use Orchestra\Testbench\TestCase;
use Smorken\Ext\Controller\BaseController;
use Smorken\Ext\Controller\Traits\Index;
use Smorken\Ext\Controller\Traits\Provider;

class MTController extends BaseController
{

    use Index, Provider;

    protected $base_view = 'smorken/ext-controller::testing';

    public function __construct(MTProvider $provider)
    {
        $this->setProvider($provider);
        parent::__construct();
    }
}

class MTProvider
{

}

class MissingTraitControllerTest extends TestCase
{

    /**
     * @var m\Mock
     */
    protected $provider;

    public function setUp(): void
    {
        parent::setUp();

        $this->provider = m::mock(MTProvider::class);
        $this->app[MTProvider::class] = $this->provider;
        $this->addRoutes();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testMissingTraitIsException()
    {
        $response = $this->get('/');
        $response->assertSeeText('Smorken\Ext\Controller\Traits\Model is required');
    }

    protected function addRoutes()
    {
        Route::group(['middleware' => ['web']], function () {
            Route::get('', '\Tests\Smorken\Ext\Controller\e2e\MTController@index');
        });
    }
}

<?php

namespace Tests\Smorken\Ext\Controller;

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/7/16
 * Time: 11:02 AM
 */
abstract class ProjectTest extends \Orchestra\Testbench\TestCase
{

    public function setUp(): void
    {
        parent::setUp();
        view()->addNamespace('smorken/ext-controller', __DIR__ . '/../views');
    }

    protected function getPackageProviders($app)
    {
        return [
            \Smorken\Ext\Controller\ServiceProvider::class,
        ];
    }
}

<?php
return [
    'icons' => [
        'view'   => 'text-primary fa fa-eye',
        'update' => 'text-success fa fa-pencil',
        'delete' => 'text-danger fa fa-trash',
    ],
];
